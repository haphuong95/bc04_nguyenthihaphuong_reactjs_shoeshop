import React, { Component } from "react";
import { dataShoe } from "./data_shoe";
import GioHang from "./GioHang";
import ListShoe from "./ListShoe";
import DetailShoe from "./DetailShoe";

export default class Ex_ShoeShop extends Component {
  state = {
    shoeArr: dataShoe,
    detailShoe: dataShoe[0],
    gioHang:[],
  };
  handleXemChiTiet=(idShoe)=>{
    // cách 1
    let index=this.state.shoeArr.findIndex((item)=>{
      return item.id==idShoe;
    });
    // if(index!==-1){}
    index!==-1 &&
    this.setState({
      detailShoe:this.state.shoeArr[index],
    });
  };
  handleAddToCart=(shoe)=>{
    // soLuong 
    let cloneGioHang=[...this.state.gioHang];
    // cloneGioHang.push(shoe);
    let index=this.state.gioHang.findIndex((item)=>{
      return item.id==shoe.id;
    });

    // TH_1: sản phẩm chưa có trong giỏ hàng 
    if(index ==-1){
      let spGioHang={...shoe, soLuong : 1};
      console.log('spGioHang: ', spGioHang);
      cloneGioHang.push(spGioHang);
    } else {
    // TH_2: sản phẩm đã có trong giỏ hàng 
      // let sp=cloneGioHang[index];
      // // tăng số lượng sản phẩm lên 1 đơn vị 
      // sp.soLuong++;
      // cloneGioHang[index]=sp;
      cloneGioHang[index].soLuong++;
    }

    this.setState(
    {
      gioHang:cloneGioHang,
    },
    ()=>{
      // tự động chạy sau khi setState
      console.log(this.state);
    }
    );
  };
  handleRemove=(idShoe)=>{
    console.log('idShoe: ', idShoe);
    let index=this.state.gioHang.findIndex((item)=>{
      return item.id==idShoe;
    })

}
  render() {
    // console.log(this.state.gioHang.length);
    return (
      <div>
        <GioHang 
        handleRemove= {this.handleRemove}
        gioHang={this.state.gioHang}/>
        <ListShoe 
        data={this.state.shoeArr} 
        handleXemChiTiet={this.handleXemChiTiet} 
        handleAddToCart={this.handleAddToCart}
        />
        <DetailShoe detailShoe={this.state.detailShoe} />
      </div>
    );
  }
}
